import React, {Component, PropTypes} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Header from '../components/Header';
import MainSection from '../components/MainSection';
import * as TodoActions from '../actions/todos';
import Drawer from 'material-ui/Drawer';
import {MenuItem, Divider, TextField,RaisedButton} from 'material-ui';
// For Customization Options, edit  or use
// './src/material_ui_raw_theme_file.jsx' as a template.
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from '../src/material_ui_raw_theme_file'
import History from 'material-ui/svg-icons/action/history';
import Calendar from 'material-ui/svg-icons/action/date-range';
import Person from 'material-ui/svg-icons/social/person';
import Mail from 'material-ui/svg-icons/communication/mail-outline';
require('../src/styles.css');
var img = require('../src/img/logo3.png');
class App extends Component {

  constructor() {
    super();
    this.state = {
      open: false,
      loggedIn: false
    }
  }

  render() {
    const {todos, actions} = this.props;
    if (!this.state.loggedIn) {
      return (
        <div>
          <MuiThemeProvider muiTheme={theme}>
            <div className='login'>
              <div className='login-container'>
                <div className='login-overlay'></div>
                <img src={img} className='logo' width="150"/>
                <TextField hintText="Username" floatingLabelText="Username"/><br/>
                <TextField hintText="Password" floatingLabelText="Password" type="password"/><br/>
<RaisedButton label="Login" style={{marginTop:50}} fullWidth={true} onClick={()=>{this.setState({loggedIn:true})}}/>
              </div>

            </div>
          </MuiThemeProvider>
        </div>
      );
    } else {
      return (
        <div>
          <MuiThemeProvider muiTheme={theme}>
            <div>
              <Header
                leftTap={() => {
                this.setState({open: true})
              }}/>
              <Drawer
                docked={false}
                width={300}
                open={this.state.open}
                onRequestChange={(open) => this.setState({open})}>
                <MenuItem leftIcon={< Person />}>My Profile</MenuItem>
                <MenuItem leftIcon={< Calendar />}>Calendar</MenuItem>
                <MenuItem leftIcon={< History />}>History</MenuItem>
                <MenuItem leftIcon={< Mail />}>Messages</MenuItem>
                <Divider/>
                <MenuItem>Work</MenuItem>
                <MenuItem>Murphy</MenuItem>
                <MenuItem>Reporting</MenuItem>
                <MenuItem>Setup</MenuItem>
              </Drawer>
            </div>
          </MuiThemeProvider>
        </div>
      );
    }

  }
}

App.propTypes = {
  todos: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {todos: state.todos};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
