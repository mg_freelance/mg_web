import React, { PropTypes, Component } from 'react';
import TodoTextInput from './TodoTextInput';

import AppBar from 'material-ui/AppBar';

const defaultStyle = {
  marginLeft: 20
};

class Header extends Component {
 
  render() {
    return (
      <header className="header">
          <AppBar title="Welcome" onLeftIconButtonTouchTap={()=>{this.props.leftTap()}} />
         
      </header>
    );
  }
}

export default Header;
